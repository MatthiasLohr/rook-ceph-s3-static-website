# rook-ceph S3 Static Website

S3 buckets can be used to host static websites.
This tutorial explains how static website hosting can be set up for [Ceph](https://ceph.com/) clusters managed by [rook.io](https://rook.io/).
Further, we provide a helm chart which can be used to easily prepare buckets and ingresses for websites hosted this way after the [initial setup](#initial-setup) has be done.


## Setup

This section describes how to set up the static website hosting feature.
After this setup has completed, our Helm chart can be used for easy deployment of buckets for hosting static websites.


### Prequisites

For this tutorial, we assume the following prequisites to be met:

  * [Kubernetes](https://kubernetes.io/) cluster up and running.
  * Running [Ceph](https://ceph.com/) cluster created using [rook.io](https://rook.io/).


### Initial Setup

  * Decide for two distinct (sub)domains, one for S3 without static website support, one for buckets serving static websites.
    In this tutorial, we use `s3.example.org` and `s3website.example.org`.
    **Caution:** The domains **must not** overlap.
    E.g., `s3.example.org` and `website.s3.example.org` will not work.
  * Set DNS records for the domains above as follows:
    ```
    s3.example.org. IN A <Kubernetes public LoadBalancer IPv4 address>
    s3.example.org. IN AAAA <Kubernetes public LoadBalancer IPv6 address>
    *.s3.example.org. IN CNAME s3.example.org.
    s3website.example.org. IN A <Kubernetes public LoadBalancer IPv4 address>
    s3website.example.org. IN AAAA <Kubernetes public LoadBalancer IPv6 address>
    *.s3website.example.org. IN CNAME s3website.example.org.
    ```
  * Use the [`configOverride`](https://rook.io/docs/rook/latest-release/Helm-Charts/ceph-cluster-chart/?h=configoverride#configuration) feature of the `rook-ceph-cluster` Helm chart, or create the `rook-config-override` ConfigMap manually to set the following Ceph settings:
    ```
    [client]
    rgw_enable_static_website = true
    rgw_dns_name = s3.example.org
    rgw_dns_s3website_name = s3website.example.org
    rgw_resolve_cname = true
    ```


### Website Bucket Setup

After the [initial setup](#initial-setup) has been completed, our helm chart can be used to create the resources needed for a bucket for your static website:

  * Add the Helm chart repository:
    ```
    helm repo add mlohr https://helm-charts.mlohr.com/
    helm repo update
    ```
  * Decide for one domain for the static website.
    In this tutorial, we use `static-website.example.org`.
    **Caution:** This domain **must not** be a subdomain of the domains used for the S3 services. E.g., `static-website.s3.example.org` will not work.
  * Use the helm chart to deploy the required resources to the Kubernetes cluster.
    There are two options, either using `CNAME` or `A`/`AAAA` DNS records for `static-website.example.org`:
    * `CNAME` (recommended):
      ```
      helm upgrade --install static-website mlohr/rook-ceph-s3-static-website \
        --set ingress.host=static-website.example.org \
        --set bucket.useIngressHostAsName=false
      ```
    * `A`/`AAAA` (to be used when no `CNAME` record can be created):
      ```
      helm upgrade --install static-website mlohr/rook-ceph-s3-static-website \
        --set ingress.host=static-website.example.org \
        --set bucket.useIngressHostAsName=true
      ```
  * After the helm chart has been deployed, it will provide you with further instructions how to proceed.

The steps above can be repeated for multiple static websites hosted on S3 buckets.


## Open ToDos

  * The configuration override currently addresses all clients, as we were not able to figure out a more specific matching section to use.
    If you know, please [open a ticket](https://gitlab.com/MatthiasLohr/rook-ceph-s3-static-website/-/issues/new)!


## References

  * https://gist.github.com/robbat2/ec0a66eed28e5f0e1ef7018e9c77910c


## License

This project is published under the Apache License, Version 2.0.
See [LICENSE.md](https://gitlab.com/MatthiasLohr/gitlab-agent-permissioned-helm-chart/-/blob/main/LICENSE.md) for more information.

Copyright (c) by [Matthias Lohr](https://mlohr.com/) &lt;[mail@mlohr.com](mailto:mail@mlohr.com)&gt;
